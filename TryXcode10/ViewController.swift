//
//  ViewController.swift
//  TryXcode10
//
//  Created by satoutakeshi on 2018/07/30.
//  Copyright © 2018年 Personal Factory. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet private weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = "titleLabel"
        }
    }

    @IBOutlet private weak var bodyLable: UILabel! {
        didSet {
            bodyLable.text = "bodyLable"
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        print("Change comment")
    }
}

